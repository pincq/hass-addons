#!/bin/bash
folder_id=$1
monitor_path=$2

files=$(cd $monitor_path && /usr/bin/find . -maxdepth 1 -type f -not -path '*/\.*' | /usr/bin/sed 's/^\.\///g' )
if [ $folder_id -gt 0 ]; then
    files2=$(/root/.cargo/bin/pcloud-cli folder $folder_id list | /usr/bin/grep file | /usr/bin/awk '{print $3}')
    for entry in $files2
    do
        echo "File name ${entry}"
        inarray=$(echo ${files[@]} | /usr/bin/grep -ow "$entry" | wc -w)
        if [ $inarray -eq 0 ]; then
            file_id=$(/root/.cargo/bin/pcloud-cli folder $folder_id list | /usr/bin/grep $entry | /usr/bin/awk '{print $1}')
            echo "Delete File with ID ${folder_id}"
            /root/.cargo/bin/pcloud-cli file delete $file_id >/dev/null 2>&1
        else
            echo "Keep file"
        fi
        echo "******************************"
    done
fi