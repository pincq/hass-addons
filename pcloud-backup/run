#!/usr/bin/with-contenv bashio
# ==============================================================================

pcloud_username="$(bashio::config 'pcloud_username')"
pcloud_password="$(bashio::config 'pcloud_password')"
pcloud_region="$(bashio::config 'pcloud_region')"
pcloud_path="$(bashio::config 'pcloud_path')"
delete_local_backups="$(bashio::config 'delete_local_backups' 'true')"
local_backups_to_keep="$(bashio::config 'local_backups_to_keep' '3')"
monitor_path="/backup"
jq_filter=".backups|=sort_by(.date)|.backups|reverse|.[$local_backups_to_keep:]|.[].slug"

bashio::log.info "=============================================================================="
bashio::log.info "Config Dump ..."
bashio::log.info "Username: ${pcloud_username}"
bashio::log.info "Region: ${pcloud_region}"
bashio::log.info "Remote Path: ${pcloud_path}"
bashio::log.info "Local Backups to keep: ${local_backups_to_keep}"
bashio::log.info "Delete Local Backups: ${delete_local_backups}"

bashio::log.info "=============================================================================="
bashio::log.info "Write credentials file"

/usr/bin/cat >/root/.config/pcloud.json <<EOL
{
    "credentials": {
            "username": "${pcloud_username}",
            "password": "${pcloud_password}"
    },
    "region": {
            "name": "${pcloud_region}"
    }
}
EOL

bashio::log.info "=============================================================================="
bashio::log.info "Create remote backup folder, if not exist"

IFS="/" read -a subfolders <<< $pcloud_path
folder_id=0
for (( i=0; i<${#subfolders[@]}; i++ )); do
    parent_folder_id=$folder_id
    folder_id=$(/root/.cargo/bin/pcloud-cli folder $parent_folder_id list | /usr/bin/grep ${subfolders[$i]} | /usr/bin/awk '{print $1}')
    if ! [ -n "$folder_id" ] && ! [ $folder_id -gt 0 ]; then 
        folder_id=$(/root/.cargo/bin/pcloud-cli folder $parent_folder_id create ${subfolders[$i]} | /usr/bin/awk '{print $NF}')
    fi
done
bashio::log.info "Folder ID is ${folder_id}"

bashio::log.info "=============================================================================="
bashio::log.info "Upload backup files"
/opt/upload.sh $folder_id $monitor_path


bashio::log.info "=============================================================================="
bashio::log.info "Delete old remote backup files"
/opt/delete.sh $folder_id $monitor_path

if bashio::var.true "${delete_local_backups}"; then
    bashio::log.info "=============================================================================="
    bashio::log.info "Will delete local backups except the '${local_backups_to_keep}' newest ones."
    backup_slugs="$(bashio::api.supervisor "GET" "/backups" "false" "$jq_filter")"
    bashio::log.info "Backups to delete: '$backup_slugs'"

    for s in $backup_slugs; do
        bashio::log.info "Deleting Backup: '$s'"
        bashio::api.supervisor "DELETE" "/backups/$s"
    done
else
    bashio::log.info "Will not delete any local backups since 'delete_local_backups' is set to 'false'"
fi

bashio::log.info "=============================================================================="
bashio::log.info "Finished PCloud Backup."