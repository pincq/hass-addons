import requests
import json

def get_json_from_url(url):
    try:
        response = requests.get(url)

        # statuscode 200
        if response.status_code == 200:
            # json export
            json_data = response.json()
            return json_data
        else:
            print(f"Fehler beim Abrufen der Daten. Statuscode: {response.status_code}")
    except Exception as e:
        print(f"Fehler: {e}")

def download_file(url, local_filename):
    try:
        # Anfrage an die URL senden
        response = requests.get(url)

        # Überprüfen, ob die Anfrage erfolgreich war (Statuscode 200)
        if response.status_code == 200:
            # Inhalt der Datei in lokaler Datei speichern
            with open(local_filename, 'wb') as local_file:
                local_file.write(response.content)
            print(f"Die Datei wurde erfolgreich unter '{local_filename}' gespeichert.")
        else:
            print(f"Fehler beim Herunterladen der Datei. Statuscode: {response.status_code}")
    except Exception as e:
        print(f"Fehler: {e}")

# gitlabe runner release page
url="https://gitlab.com/gitlab-org/gitlab-runner/-/releases.json"

# load file
json_data = get_json_from_url(url)

# parse version
if json_data:
    latest_version=json.dumps(json_data[0].get("name"), indent=2).strip('"')
    print("Latest Version: ")
    print(latest_version)

# load local addon config
local_version_file = '../config.json'

with open(local_version_file, 'r') as file:
    json_data2 = json.load(file)

# parse version
current_version=json.dumps(json_data2.get("version"), indent=2).strip('"')   
print("Current Version: ")
print(current_version)

# Beispiel-URL für die zu ladende Datei
url = f"https://gitlab-runner-downloads.s3.amazonaws.com/{latest_version}/deb/gitlab-runner_amd64.deb"
print(url)

# Lokaler Dateiname, unter dem die Datei gespeichert werden soll
local_filename = "gitlab-runner_amd64.deb"

# Funktion aufrufen, um die Datei herunterzuladen und lokal zu speichern
if current_version != latest_version:
    download_file(url, local_filename)

# Dateipfad und Name
file_path = "version.txt"

# Öffne die Datei im Schreibmodus ('w' für write)
with open(file_path, 'w') as file:
    # Schreibe den Text in die Datei
    file.write(latest_version)